const Course = require("../models/Course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.addCourse = (data) => {
	if (data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return newCourse.save().then((newCourse, error) => {
			if(error) {
				return `Course cannot be added. Something went wrong.`
			}
			return `Course ${data.course.name} has been successfully created.`
		})
	}

	let message = Promise.resolve("User must be ADMIN to access this.")

	return message.then((value) => {
		return value
	})
}